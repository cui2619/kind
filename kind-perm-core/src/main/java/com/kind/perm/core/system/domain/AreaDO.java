package com.kind.perm.core.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kind.common.persistence.PageQuery;
import java.util.Date;
/**
 * 区域信息<br/>
 *
 * @Date: 2017-03-01 09:53:29
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public class AreaDO extends PageQuery {
	
		/** */	private java.lang.Integer id;	/** */	private java.lang.String code;	/** */	private java.lang.String name;	/** */	private java.lang.String pcode;	/** */	private java.lang.String shortName;	/** */	private java.lang.String levelType;	/** */	private java.lang.String cityCode;	/** */	private java.lang.String zipCode;	/** */	private java.lang.String mergerName;	/** */	private java.lang.String lng;	/** */	private java.lang.String lat;	/** */	private java.lang.String pinyin;	public java.lang.Integer getId() {	    return this.id;	}	public void setId(java.lang.Integer id) {	    this.id=id;	}	public java.lang.String getCode() {	    return this.code;	}	public void setCode(java.lang.String code) {	    this.code=code;	}	public java.lang.String getName() {	    return this.name;	}	public void setName(java.lang.String name) {	    this.name=name;	}	public java.lang.String getPcode() {	    return this.pcode;	}	public void setPcode(java.lang.String pcode) {	    this.pcode=pcode;	}	public java.lang.String getShortName() {	    return this.shortName;	}	public void setShortName(java.lang.String shortName) {	    this.shortName=shortName;	}	public java.lang.String getLevelType() {	    return this.levelType;	}	public void setLevelType(java.lang.String levelType) {	    this.levelType=levelType;	}	public java.lang.String getCityCode() {	    return this.cityCode;	}	public void setCityCode(java.lang.String cityCode) {	    this.cityCode=cityCode;	}	public java.lang.String getZipCode() {	    return this.zipCode;	}	public void setZipCode(java.lang.String zipCode) {	    this.zipCode=zipCode;	}	public java.lang.String getMergerName() {	    return this.mergerName;	}	public void setMergerName(java.lang.String mergerName) {	    this.mergerName=mergerName;	}	public java.lang.String getLng() {	    return this.lng;	}	public void setLng(java.lang.String lng) {	    this.lng=lng;	}	public java.lang.String getLat() {	    return this.lat;	}	public void setLat(java.lang.String lat) {	    this.lat=lat;	}	public java.lang.String getPinyin() {	    return this.pinyin;	}	public void setPinyin(java.lang.String pinyin) {	    this.pinyin=pinyin;	}
}

