package com.kind.perm.core.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.dao.OrganizationDao;
import com.kind.perm.core.system.domain.OrganizationDO;
import com.kind.perm.core.system.service.OrganizationService;

/**
 * 
 * 机构信息业务处理实现类. <br/>
 *
 * @date:2017-03-02 09:24:35 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationDao organizationDao;

	@Override
	public PageView<OrganizationDO> selectPageList(PageQuery pageQuery) {
        pageQuery.setPageSize(pageQuery.getPageSize());
		List<OrganizationDO> list = organizationDao.page(pageQuery);
		int count = organizationDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(OrganizationDO entity) throws ServiceException {
		try {
           return organizationDao.saveOrUpdate(entity);
        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}

	@Override
	public OrganizationDO getById(Long id) {
		return organizationDao.getById(id);
	}

    @Override
    public void remove(Long id) throws ServiceException{
        try {
            organizationDao.remove(id);

        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
    }

	@Override
	public List<OrganizationDO> queryList(OrganizationDO entity) {
		return organizationDao.queryList(entity);
	}
}