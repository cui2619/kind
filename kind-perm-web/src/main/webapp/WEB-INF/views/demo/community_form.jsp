<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/plugins/code/prettify.css" />
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainform" action="${ctx}/demo/community/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">小区基本信息</span>
								</div>
								<table class="kv-table" cellspacing="10">
									<tr>
										<td class="kv-label">市：</td>
										<td class="kv-content"><input type="hidden" name="id"
											value="${entity.id }" /> <input name="cityName" type="text"
											value="${entity.cityName}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">市编码：</td>
										<td class="kv-content"><input name="cityCode" type="text"
											value="${entity.cityCode}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">区：</td>
										<td class="kv-content"><input name="areaName" type="text"
											value="${entity.areaName}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">区编码：</td>
										<td class="kv-content"><input name="areaCode" type="text"
											value="${entity.areaCode}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">小区名称：</td>
										<td class="kv-content"><input name="community"
											type="text" value="${entity.community}"
											class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
								<!-- 上传控件 star -->
									<tr>
										<td class="kv-label">小区描述：</td>
										<td class="kv-content"><textarea name="content"
												id="citycontent" style="width: 99%; align: center;">${entity.content} </textarea>

										</td>
									</tr>

									<tr>
									<!-- 通过业务id和类型找到首图 -->
										<td class="w80px center"><img
																src="${ctx}/file/getTypeOnlyImage/${entity.id }/CommunityDO"
																style="width: 60px; height: 60px" /></td>
									
										<c:if test="${filelist!= null && fn:length(filelist) > 0}">
											<td class="kv-label">已上传附件</td>
											<td class="kv-content">
												<table>
													<c:forEach items="${filelist}" var="row">
														<tr id="annex_tr_${row.id}" url="${row.path}"
															annexID="${row.id}">
															<td class="w80px center"><img
																src="${ctx}/file/inline/${row.path}/${row.uuid}/${row.name}/${row.ext}"
																style="width: 60px; height: 60px" /></td>
															<td class="w80px center"><img
																src="${ctx}/file/getImage/${row.id}"
																style="width: 60px; height: 60px" /></td>
															
															<td class="wauto annex-tdhidden">${row.name}</td>
															<td class="w80px center">${row.size}B</td>
															<td class="w140px center"><fmt:formatDate
																	value="${row.createTime}" pattern="yyyy.MM.dd HH:mm:ss" /></td>
															<td class="w80px center"><a
																href="${ctx}/file/download/${row.path}/${row.uuid}/${row.name}/${row.ext}">下载</a>
																<c:if test="${view != true }">
																<a href="javascript:removeAnnex(${row.id})">删除</a>
																</c:if>
																</td>
																
														</tr>
													</c:forEach>
												</table>
											</td>
									</tr>
									</c:if>
									<c:if test="${view != true }">
									<tr>
										<td class="kv-label">文件上传</td>
										<td class="kv-content"><input type="file"
											name="uploadify" id="file_upload" /> <input type="hidden"
											name="uploadfileIds" id="uploadfileIds" value="${fileIds}" />

											<input type="hidden" name="typefiled" id="typefiled"
											value="CommunityDO" /> <a class="easyui-linkbutton"
											onclick="startUpload();" href="javascript:void(0);">开始上传</a>
											<a
											href="javascript:$('#file_upload').uploadify('cancel', '*')"
											class="easyui-linkbutton">取消所有上传</a></td>
									</tr>
									</c:if>
								<!-- 上传控件 end -->
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script  src="${ctx}/static/plugins/kindeditor/kindeditor.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/plugins/code/prettify.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {

			$('#mainform').form({
				onSubmit : function() {
					var isValid = $(this).form('validate');
					// 返回false终止表单提交
					return isValid;
				},
				success : function(data) {
					var dataObj = eval("(" + data + ")");//转换为json对象
					if (submitSuccess(dataObj, dg, d)) {
						dg.treegrid('reload');
					}
				}
			});
		});
	</script>

	<!-- 文件上传 star -->
	<script type="text/javascript" charset="UTF-8">
		$(function() {
			$("#file_upload")
					.uploadify(
							{
								'height' : 27,
								'width' : 80,
								'buttonText' : '添加附件',
								'swf' : '${ctx}/static/plugins/uploadify/uploadify.swf?ver='
										+ Math.random(),
								'uploader' : '${ctx}/file/uploadFile',
								'auto' : false,
								'fileTypeExts' : '*.*',
								'formData' : {
									'type' : '1234'
								},
								//在浏览窗口底部的文件类型下拉菜单中显示的文本
								'fileTypeDesc' : '支持的格式：',
								//允许上传的文件后缀
								'fileTypeExts' : '*.jpg;*.jpge;*.gif;*.png',
								'removeCompleted' : false,
								'onUploadStart' : function(file, data, response) {
									$("#file_upload").uploadify("settings",
											"formData", {
												'type' : '1234'
											});
								},
								'onUploadSuccess' : function(file, data,
										response) {

									if ($('#uploadfileIds').val() == "") {
										$('#uploadfileIds').val(data + ",");
									} else {
										$('#uploadfileIds').val(
												$('#uploadfileIds').val()
														+ data + ",");
									}

								},
								'onQueueComplete' : function(queueData) {
									$('#uploader_msg').html(
											queueData.uploadsSuccessful
													+ '个文件上传成功。');
								}
							});
		});
		function startUpload() {
			//校验     
			$('#file_upload').uploadify('upload', '*');
		}

		//删除
		function removeAnnex(ids) {
			$.messager.confirm('提示', '删除后无法恢复，您确定要删除吗？', function(data) {
				if (data) {
					$.ajax({
						type : 'get',
						url : "${ctx}/file/deletefile/" + ids,
						success : function(data) {
							$('#annex_tr_' + ids).remove();

						}
					});
				}
			});
		}
	</script>
	<!-- 文件上传 end -->
	<!-- 富文本 star -->
	<script>
		$(function() {
			var citycontent = KindEditor.create('#citycontent', {
				width : "500px",
				height : "180px",
				items : [ 'source', '|', 'undo', 'redo', '|', 'preview',
						'print', 'template', 'code', 'cut', 'copy', 'paste',
						'plainpaste', 'wordpaste', '|', 'justifyleft',
						'justifycenter', 'justifyright', 'justifyfull',
						'insertorderedlist', 'insertunorderedlist', 'indent',
						'outdent', 'subscript', 'superscript', 'clearhtml',
						'quickformat', 'selectall', '|', 'fullscreen',
						'formatblock', 'fontname', 'fontsize', '|',
						'forecolor', 'hilitecolor', 'bold', 'italic',
						'underline', 'strikethrough', 'lineheight',
						'removeformat'],
				 allowFileManager : true,  
                 allowImageUpload : true,   
				afterBlur : function() {
					//数据同步
					this.sync();
				},
				afterCreate : function() {

				},
				afterChange : function() {
					this.sync();
				}
			});
		}); 
	</script>
	
	<!-- 富文本 end -->
</body>
</html>